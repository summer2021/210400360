package cn.piflow.bundle.stream

import cn.piflow.bundle.util.{SensorReading, SensorSource, SensorTimeAssigner}
import cn.piflow.{JobContext, JobInputStream, JobOutputStream, ProcessContext}
import cn.piflow.conf.{ConfigurableStop, Port, StopGroup}
import cn.piflow.conf.bean.PropertyDescriptor
import cn.piflow.conf.util.ImageUtil
import org.apache.flink.streaming.api.scala.{DataStream, StreamExecutionEnvironment}
import org.apache.flink.api.scala._

class SensorSourceStop extends ConfigurableStop{
  override val authorEmail: String = "xjzhu@cnic.cn"
  override val description: String = "Sensor reading"
  override val inportList: List[String] = List()
  override val outportList: List[String] = List(Port.DefaultPort)

  override def setProperties(map: Map[String, Any]): Unit = {}

  override def getPropertyDescriptor(): List[PropertyDescriptor] = {
    List()
  }

  override def getIcon(): Array[Byte] = {
    ImageUtil.getImage("icon/hive/SelectHiveQL.png")
  }

  override def getGroup(): List[String] = {
    List(StopGroup.HiveGroup)
  }

  override def initialize(ctx: ProcessContext): Unit = {}

  override def perform(in: JobInputStream, out: JobOutputStream, pec: JobContext): Unit = {
    val env = pec.get[StreamExecutionEnvironment]()
    val sensorData : DataStream[SensorReading] = env.addSource(new SensorSource)
      .assignTimestampsAndWatermarks(new SensorTimeAssigner)
    out.write(sensorData)

    //env.execute("SensorSourceStop")
  }
}
