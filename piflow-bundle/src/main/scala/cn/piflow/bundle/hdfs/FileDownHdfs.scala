package cn.piflow.bundle.hdfs

import java.io.InputStream
import java.net.{HttpURLConnection, URL}
import cn.piflow.conf._
import cn.piflow.conf.bean.PropertyDescriptor
import cn.piflow.conf.util.{ImageUtil, MapUtil}
import cn.piflow.{JobContext, JobInputStream, JobOutputStream, ProcessContext}
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FSDataOutputStream, FileSystem, Path}



import org.apache.flink.api.scala.ExecutionEnvironment
import org.apache.flink.configuration.Configuration
import org.apache.flink.api.scala._
import org.apache.flink.api.common.typeinfo.TypeHint
import org.apache.flink.api.common.typeinfo.TypeInformation
import org.apache.flink.table.api.Table
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment
import org.apache.flink.streaming.api.functions.sink.filesystem.StreamingFileSink
import org.apache.flink.streaming.api.scala.DataStream
import org.apache.flink.streaming.api.scala.DataStreamUtils
import org.apache.flink.core.fs.Path
import org.apache.flink.core.fs.FSDataInputStream
import org.apache.flink.core.fs.FSDataOutputStream


class FileDownHdfs extends ConfigurableStop{

  val authorEmail: String = "yangqidong@cnic.cn"
  val description: String = "Download the data from the url to HDFS"
  val inportList: List[String] = List(Port.DefaultPort)
  val outportList: List[String] = List(Port.DefaultPort)

  var hdfsUrl:String =_
  var hdfsPath:String =_
  var url_str:String=_

  def perform(in: JobInputStream, out: JobOutputStream, pec: JobContext): Unit = {

    //创建批处理环境
    var env = ExecutionEnvironment.getExecutionEnvironment

    val configuration: Configuration = new Configuration()
    configuration.set("fs.defaultFS",hdfsUrl)

    val url=new URL(url_str)
    val uc:HttpURLConnection=url.openConnection().asInstanceOf[HttpURLConnection]
    uc.setDoInput(true)
    uc.connect()

    val inputStream:DataStream[String] = uc.getInputStream()
    //val inputData :DataSet[String] = uc.getInputStream()

    val sink: StreamingFileSink[String] = StreamingFileSink
      .forRowFormat(new Path(hdfsUrl+hdfsPath), new SimpleStringEncoder[String]("UTF-8")) // 所有数据都写到同一个路径
      .build()

    inputStream.addSink(sink)
    inputStream.setParallelism(1)

    env.execute("FileDownHdfs")
  }

  def initialize(ctx: ProcessContext): Unit = {

  }

  def setProperties(map: Map[String, Any]): Unit = {
    hdfsUrl=MapUtil.get(map,key="hdfsUrl").asInstanceOf[String]
    hdfsPath=MapUtil.get(map,key="hdfsPath").asInstanceOf[String]
    url_str=MapUtil.get(map,key="url_str").asInstanceOf[String]
  }

  override def getPropertyDescriptor(): List[PropertyDescriptor] = {
    var descriptor : List[PropertyDescriptor] = List()

    val url_str = new PropertyDescriptor()
      .name("url_str")
      .displayName("Url_Str")
      .description("Network address of file")
      .defaultValue("")
      .required(true)
    descriptor = url_str :: descriptor

    val hdfsPath = new PropertyDescriptor()
      .name("hdfsPath")
      .displayName("HdfsPath")
      .defaultValue("")
      .description("File path of HDFS")
      .required(true)
      .example("/work/test.gz")
    descriptor = hdfsPath :: descriptor

    val hdfsUrl = new PropertyDescriptor()
      .name("hdfsUrl")
      .displayName("HdfsUrl")
      .defaultValue("")
      .description("URL address of HDFS")
      .required(true)
      .example("hdfs://192.168.3.138:8020")
    descriptor = hdfsUrl :: descriptor

    descriptor
  }

  override def getIcon(): Array[Byte] = {
    ImageUtil.getImage("icon/http/LoadZipFromUrl.png")
  }

  override def getGroup(): List[String] = {
    List(StopGroup.HdfsGroup)
  }


}
