package cn.piflow.bundle.hdfs

import cn.piflow._
import cn.piflow.conf.bean.PropertyDescriptor
import cn.piflow.conf.util.{ImageUtil, MapUtil}
import cn.piflow.conf.{ConfigurableStop, Port, StopGroup}
import org.apache.flink.api.common.state.StateTtlConfig.{DISABLED, TtlTimeCharacteristic}
import org.apache.flink.api.java.io.TextInputFormat
import org.apache.flink.api.common.typeinfo.TypeHint
import org.apache.flink.api.common.typeinfo.TypeInformation
import org.apache.flink.table.api.Table
import org.apache.flink.configuration.Configuration
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment
import org.apache.flink.streaming.api.functions.sink.filesystem.StreamingFileSink
import org.apache.flink.streaming.api.scala
import org.apache.flink.streaming.api.scala.DataStream
import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment
import org.apache.flink.core.fs.Path
import org.apache.flink.api.scala._
import org.apache.flink.streaming.api.datastream.DataStreamSource
import org.apache.orc.OrcFile
import org.apache.orc.Reader
import org.apache.orc.RecordReader
import org.apache.orc.TypeDescription

class GetHdfs extends ConfigurableStop{
  override val authorEmail: String = "ygang@cnic.com"
  override val description: String = "Get data from hdfs"
  override val inportList: List[String] = List(Port.DefaultPort)
  override val outportList: List[String] = List(Port.DefaultPort)

  var hdfsUrl : String=_
  var hdfsPath :String= _
  var types :String = _

  override def perform(in: JobInputStream, out: JobOutputStream, pec: JobContext): Unit = {

    //创建批处理环境
    //val env = ExecutionEnvironment.getExecutionEnvironment
    //创建流处理环境
    val env = StreamExecutionEnvironment.getExecutionEnvironment
    env.enableCheckpointing(10000)
    env.setParallelism(1)
    env.enableCheckpointing(5000)
    val path = hdfsUrl+hdfsPath
    val source = env.addSource(path)
    val configuration: Configuration = new Configuration()
    configuration.set("fs.defaultFS",hdfsUrl)

    if (types == "json") {
        val input = env.readFile(new TextInputFormat(new Path(path)), path)
        println(input)
        out.write(input)
      } else if (types == "csv") {
        val input = env.readCsvFile(path)
        println(input)
        out.write(input)
      //val sensorData : DataStream[SensorReading] = env.addSource(new SensorSource)
//      .assignTimestampsAndWatermarks(new SensorTimeAssigner)
      }else if (types == "parquet") {
        val df = env.readFile((new TextInputFormat(new Path(path)),path)
        val job = Job.getInstance()
        val dIf = new HadoopInputFormat[Void, AvroTamAlert](new AvroParquetInputFormat(), classOf[Void], classOf[AvroTamAlert], job)
        FileInputFormat.addInputPath(job, new Path(path))
        val dataset = env.createInput(dIf)
        println(dataset.count())
        out.write(df)

      }else if (types == "orc"){
        val input: DataStream[String] = ...
      val props = new Properties()
      props.setProperty("orc.compress", "SNAPPY")
      props.setProperty("orc.bloom.filter.columns", "x")

      val schemaString = """struct<x:int,y:string,z:string>"""
      val schema = TypeDescription.fromString(schemaString)

      input
        .addSink(StreamingFileSink
          .forBulkFormat(
            new Path(out),
            OrcWriters
              .withCustomEncoder[(Int, String, String)](new Encoder, schema, props)
          )
          .withBucketAssigner(new BucketAssigner)
          .build())
        //val df = env.readFile(new TextInputFormat(new Path(path)),path)
        //df.schema.printTreeString()

        out.write(input)
      }
      else {
        val  input = env.readTextFile(path,"utf-8")
                        .getExecutionConfig()
        env.setParallelism(1)
        System.out.println(input)
        out.write(input)
    }
    env.execute("Get Hdfs")

  }
  override def setProperties(map: Map[String, Any]): Unit = {
    hdfsUrl = MapUtil.get(map,key="hdfsUrl").asInstanceOf[String]
    hdfsPath = MapUtil.get(map,key="hdfsPath").asInstanceOf[String]
    types = MapUtil.get(map,key="types").asInstanceOf[String]
  }

  override def getPropertyDescriptor(): List[PropertyDescriptor] = {
    var descriptor : List[PropertyDescriptor] = List()
    val hdfsPath = new PropertyDescriptor()
      .name("hdfsPath")
      .displayName("HdfsPath")
      .defaultValue("")
      .description("File path of HDFS")
      .required(true)
      .example("/work/")
    descriptor = hdfsPath :: descriptor

    val hdfsUrl = new PropertyDescriptor()
      .name("hdfsUrl")
      .displayName("HdfsUrl")
      .defaultValue("")
      .description("URL address of HDFS")
      .required(true)
      .example("hdfs://192.168.3.138:8020")
    descriptor = hdfsUrl :: descriptor

    val types = new PropertyDescriptor().
      name("types")
      .displayName("Types")
      .description("The type of file you want to load")
      .defaultValue("csv")
      .allowableValues(Set("txt","parquet","csv","json","orc"))
      .required(true)
        .example("csv")
    descriptor = types :: descriptor

    descriptor
  }

  override def getIcon(): Array[Byte] = {
    ImageUtil.getImage("icon/hdfs/GetHdfs.png")
  }

  override def getGroup(): List[String] = {
    List(StopGroup.HdfsGroup)
  }

  override def initialize(ctx: ProcessContext): Unit = {

  }
}
