package cn.piflow.bundle.hdfs

import cn.piflow._
import cn.piflow.conf.bean.PropertyDescriptor
import cn.piflow.conf.util.{ImageUtil, MapUtil}
import cn.piflow.conf.{ConfigurableStop, Port, StopGroup}
import org.apache.flink.api.common.serialization.SimpleStringEncoder
import org.apache.flink.configuration.Configuration
import org.apache.flink.core.fs.Path
import org.apache.flink.streaming.api.functions.sink.filesystem.StreamingFileSink
import org.apache.flink.streaming.api.scala.DataStream
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileStatus, FileSystem, Path}
import org.apache.hadoop.fs.FSDataInputStream
import org.apache.hadoop.io.IOUtils

import scala.collection.mutable.ArrayBuffer



class ListHdfs extends ConfigurableStop{

  override val authorEmail: String = "ygang@cnic.com"
  override val description: String = "Retrieve a list of files from hdfs"
  override val inportList: List[String] = List(Port.DefaultPort.toString)
  override val outportList: List[String] = List(Port.DefaultPort.toString)

  var hdfsPath :String= _
  var hdfsUrl :String= _
  var pathARR:ArrayBuffer[String]=ArrayBuffer()

  override def perform(in: JobInputStream, out: JobOutputStream, pec: JobContext): Unit = {

    var env = ExecutionEnvironment.getExecutionEnvironment

    val path = new Path(hdfsPath)
    iterationFile(path.toString)

    val rows: List[Row] = pathARR.map(each => {
      var arr:Array[String]=Array(each)
      val row: Row = Row.fromSeq(arr)
      row
    }).toList
    val configuration: Configuration = new Configuration()
    configuration.set("fs.defaultFS",hdfsUrl)
    FileSystem fs = FileSystem.get(configuration)
    FileStatus[] fileStatuses = fs.listStatus(path)
    for(FileStatus fileStatus :fileStatuses){
       boolean isdir = fileStatus.isDirectory()
       String fullpath = fileStatus.getPath.toString()
       println(fullpath)
     }

    val sensorData : DataStream[SensorReading] = env.addSource(new SensorSource)
      .assignTimestampsAndWatermarks(new SensorTimeAssigner)
    val sink: StreamingFileSink[String] = StreamingFileSink.forRowFormat(
      new Path(path),
      new SimpleStringEncoder[String]("UTF-8"))
      .build()
    sensorData.addSink(sink)
    out.write(sensorData)

    env.execute("ListHdfs")
  }

  // recursively traverse the folder
  def iterationFile(path: String):Unit = {
    val config = new Configuration()
    config.set("fs.defaultFS",hdfsUrl)
    val fs = FileSystem.get(config)
    val listf = new Path(path)

    val statuses: Array[FileStatus] = fs.listStatus(listf)

    for (f <- statuses) {
      val fsPath = f.getPath().toString
      if (f.isDirectory) {
        iterationFile(fsPath)
      } else{
        pathARR += f.getPath.toString
      }
    }

  }

  override def setProperties(map: Map[String, Any]): Unit = {
    hdfsUrl = MapUtil.get(map,key="hdfsUrl").asInstanceOf[String]
    hdfsPath = MapUtil.get(map,key="hdfsPath").asInstanceOf[String]
  }

  override def getPropertyDescriptor(): List[PropertyDescriptor] = {
    var descriptor : List[PropertyDescriptor] = List()
    val hdfsPath = new PropertyDescriptor()
      .name("hdfsPath")
      .displayName("HdfsPath")
      .defaultValue("")
      .description("File path of HDFS")
      .required(true)
      .example("/work/")
    descriptor = hdfsPath :: descriptor

    val hdfsUrl = new PropertyDescriptor()
      .name("hdfsUrl")
      .displayName("HdfsUrl")
      .defaultValue("")
      .description("URL address of HDFS")
      .required(true)
      .example("hdfs://192.168.3.138:8020")

    descriptor = hdfsUrl :: descriptor
    descriptor
  }

  override def getIcon(): Array[Byte] = {
    ImageUtil.getImage("icon/hdfs/ListHdfs.png")
  }

  override def getGroup(): List[String] = {
    List(StopGroup.HdfsGroup)
  }

  override def initialize(ctx: ProcessContext): Unit = {

  }

}
